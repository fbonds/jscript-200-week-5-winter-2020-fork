
// If an li element is clicked, toggle the class "done" on the <li>
// const myList = document.getElementsByClassName('today-list');

const ul = document.getElementsByTagName('ul');
for (u = 0; u < ul.length; u++) {
  const li = ul[u].getElementsByTagName('li')
  for (i = 0; i < li.length; i++) {
    const thisone = li[i];
    li[i].addEventListener('click', () => {
      thisone.classList.toggle('done');
    })
  }
}


// If a delete link is clicked, delete the li element / remove from the DOM

// const ul = document.getElementsByTagName('ul');
for (u = 0; u < ul.length; u++) {
  const li = ul[u].getElementsByTagName('li')
  const del = ul[u].getElementsByClassName('delete')
  for (i = 0; i < li.length; i++) {
    const thisone = li[i];
    del[i].addEventListener('click', () => {
      thisone.remove();
      })
    }
}



// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
const addListItem = () => {
  // e.preventDefault();
  // const input = this.parentNode.getElementsByTagName('input')[0];
  const input = document.getElementsByTagName('input')[0]
  console.log(input);
  const text = input.value; // use this text to create a new <li>
  const todoSpan = document.createElement('span');
  todoSpan.textContent=text;

  console.log(todoSpan);
  const todoA = document.createElement('a');
  todoA.className='delete';
  todoA.textContent='Delete';
  todoA.addEventListener('click', () => {
    todoA.remove();
  })
  console.log(todoA);
  const todoItem = document.createElement('li');
  todoItem.appendChild(todoSpan);
  todoItem.appendChild(todoA);
  console.log(todoItem);
  console.log(document.getElementsByTagName('ul')[0]);
  document.getElementsByTagName('ul')[0].appendChild(todoItem);

  todoSpan.addEventListener('click', () => {
    todoItem.classList.toggle('done');
  })

  todoA.addEventListener('click', () => {
    todoItem.remove();
  })
  
  
  // Finish function here

};
const additem = document.getElementsByClassName('add-item');
Array.from(additem).forEach(function(element) {
  element.addEventListener('click', addListItem);
});