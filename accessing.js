// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
// const headingEl = document.getElementById('h2');
// headingEl.innerText = "February 10 Weather Forecast, Seattle";

const h1El = document.querySelector('h1');
h1El.innerText = 'February 10 Weather Forecast, Seattle'

// Change the styling of every element with class "sun" to set the color to "orange"
const sunElements = document.getElementsByClassName('sun');
Array.from(sunElements).forEach((el) => {
    el.style.color = 'orange';
})

// Change the class of the second <li> from to "sun" to "cloudy"

const secondLi = document.getElementsByTagName('li')[1];
// secondLi.className = 'cloudy';
secondLi.classList.add('cloudy');
secondLi.classList.remove('sun');