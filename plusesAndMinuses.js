// When a user clicks the + element, the count should increase by 1 on screen.
let counter = 0;
const plusEl = document.getElementById('plus');
const counterEl = document.getElementById('counter');
plusEl.addEventListener('click', () => {
    counter++;
    // console.log(`count is ${counter}`);
    counterEl.innerText = counter;
})
// When a user clicks the – element, the count should decrease by 1 on screen.
const minusEl = document.getElementById('minus');
minusEl.addEventListener('click', () => {
    counter--;
    // console.log(`count is ${counter}`);
    counterEl.innerText = counter;
})
